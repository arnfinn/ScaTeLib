if(CMAKE_Fortran_COMPILER_ID MATCHES XL)
   add_definitions(-DVAR_XL)
elseif(CMAKE_Fortran_COMPILER_ID MATCHES GNU)
   add_definitions(-DVAR_GNU)
elseif(CMAKE_Fortran_COMPILER_ID MATCHES Cray)
   add_definitions(-DVAR_CRAY)
elseif(CMAKE_Fortran_COMPILER_ID MATCHES Intel)
   add_definitions(-DVAR_INTEL)
elseif(CMAKE_Fortran_COMPILER_ID MATCHES PGI)
   add_definitions(-DVAR_PGI)
elseif(CMAKE_Fortran_COMPILER_ID MATCHES Fujitsu)
   add_definitions(-DVAR_FUJI)
   set(CMAKE_Fortran_FLAGS_RELEASE "-O3 ")
   set(CMAKE_Fortran_FLAGS_DEBUG "-O0 -g ")

   if(ENABLE_64BIT_INTEGERS)
      set(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} -CcdII8")
   endif()
   if(ENABLE_OMP) 
     set(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} -K openmp")
   endif()
endif()

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${PROJECT_SOURCE_DIR}/cmake/custom/mpi ${PROJECT_SOURCE_DIR}/cmake/custom/fortran_standard)

if(${CMAKE_SYSTEM_NAME} STREQUAL "Darwin")
    add_definitions(-DSYS_DARWIN)
    add_definitions(-DSYS_UNIX)
    add_definitions(-DSYS_LINUX)
    # fixme: HAVE_NO_LSEEK64 should be tested by cmake
    #        not hardcoded for Mac OSX
    add_definitions(-DHAVE_NO_LSEEK64)
    # amt: This definition is a workaround for some missing strdup() used by DFT
    #      c code on OSX Lion and newer. The workaround also works on older OSX but
    #      not needed there. For now live with always on.

    # work-around for error in Macports cmake on OSX
    if(${CMAKE_HOST_SYSTEM_PROCESSOR} MATCHES "i386")
        set(CMAKE_HOST_SYSTEM_PROCESSOR x86_64)
    endif()
endif()


#Use automatic BLAS detection
if(NOT ENABLE_CRAY_WRAPPERS)
   find_package(BLAS)
endif()

#check fortran standard
include(check_fortran_standard)

#check mpi lib
include(check_mpi)

if(ENABLE_64BIT_INTEGERS)
   add_definitions(-DVAR_INT64)
endif()

if(ENABLE_OPENMP)
   add_definitions(-DVAR_OMP)
endif()

#Set the definitions for the automatically generated source files
if(ENABLE_GPU)
   add_definitions(-DVAR_OPENACC)
   set(reorder_definitions "--acc ${reorder_definitions}")
endif()
if(ENABLE_REAL_SP)
   add_definitions(-DVAR_REAL_SP)
   set(reorder_definitions "--real_sp ${reorder_definitions}")
endif()
if(ENABLE_TENSOR_CLASS)
   add_definitions(-DVAR_TENSOR_CLASS)
endif()
if(ENABLE_REQUEST_BASED_MPI3)
   add_definitions(-DUSE_REQUEST_MPI3_ROUTINES)
endif()
if(ENABLE_BRUTE_FORCE_FLUSH)
   add_definitions(-DVAR_BRUTE_FLUSH)
endif()
