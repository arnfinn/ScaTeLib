bname=$(basename "$0")
bname=${bname/.bash/}"_$CI_BUILD_REF_NAME"
##########
source /opt/mpich-3.1.4-gnu-4.8/source.bash
#
wrk=$1
#
if [ ! -d $bname ]   
then
   ./setup --fc=mpif90 --omp --mpi --type=debug --cmake-options="-DBUILDNAME=$bname" $bname
fi
#
cd $bname
#
#ctest -D Nightly --track GitLabCI -L ContinuousIntegration
ctest -D Nightly --track GitLabCI
exit $?
