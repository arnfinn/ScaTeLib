bname=$(basename "$0")
bname=${bname/.bash/}
##########

export LM_LICENSE_FILE=/opt/pgi/license.dat
source /opt/pgi/linux86-64/15.7/pgi.sh
source /opt/pgi/linux86-64/15.7/mpi.sh
export LD_LIBRARY_PATH=/opt/pgi/15.7/share_objects/lib64:$LD_LIBRARY_PATH

wrk=$1
lib=tensor_lib_$bname
#
if [ ! -d $wrk/$lib ]
then
   cd $wrk
   git clone git@gitlab.com:pett/tensor_lib.git $lib
fi
#
cd $wrk/$lib
#
if [ ! -d $wrk/$lib/$bname ]   
then
   ./setup.py --fc=mpif90 --omp --cmake-options="-DBUILDNAME=$bname" $bname
fi
#
cd $bname
#
ctest -D Nightly
