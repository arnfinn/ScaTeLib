module tensor_error_handler
   use tensor_parameters_module

   public :: tensor_status_quit
   public :: tensor_warning
   public :: tensor_stack_init
   public :: tensor_stack_free
   public :: tensor_stack_print
   public :: tensor_stack_push
   public :: tensor_stack_pop
   private

   type tensor_error_stack
      logical                                :: i = .false.
      integer                                :: n
      integer                                :: c
      character(tensor_name_length), pointer :: s(:)
      integer(tensor_mpi_kind)               :: r 
   end type tensor_error_stack

   type(tensor_error_stack), save :: tensor_stack

   character(8), parameter  :: warning_b = "WARNING("
   character(6), parameter  :: error_b   = "ERROR("
   character(2), parameter  :: error_e   = "):"

   ! interface to tensor status quit for different ints
   interface tensor_status_quit
      module procedure tensor_status_quit_std,&
                     & tensor_status_quit_long
   end interface tensor_status_quit

   contains

  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   February, 2016
  !Intent: write the error preable
  subroutine tensor_error_preamble()
     implicit none
#ifdef VAR_MPI
     print *, "Rank: ",tensor_stack%r," signals:"
#endif
  end subroutine tensor_error_preamble

  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   February, 2016
  !Intent: Initialize the error stack for the local node
  !Input:
  !       -rank: rank of the current node
  subroutine tensor_stack_init(rank)
     implicit none
     integer(tensor_mpi_kind), intent(in) :: rank
     integer :: i
     if( tensor_stack%i )then
        print *, error_b//"tensor_stack_init"//error_e//" stack is already initialized"
        stop 1
     endif
     tensor_stack%r = rank
     tensor_stack%n = 10
     tensor_stack%c =  0
     allocate(tensor_stack%s(tensor_stack%n))
     do i = 1, tensor_stack%n
        write(tensor_stack%s(i),*)""
     enddo
     tensor_stack%i = .true.
  end subroutine tensor_stack_init

  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   February, 2016
  !Intent: free the tensor stack
  subroutine tensor_stack_free()
     implicit none
     if( .not. tensor_stack%i )then
        print *, error_b//"tensor_stack_free"//error_e//" stack is not initialized"
        stop 1
     endif
     if( tensor_stack%c > 0)then
        call tensor_error_preamble()
        print *, error_b//"tensor_stack_free"//error_e//" stack counter not zero",tensor_stack%c
        stop 1
     endif
     deallocate(tensor_stack%s)
     tensor_stack%n = 0
     tensor_stack%i = .false.
  end subroutine tensor_stack_free

  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   February, 2016
  !Intent: Push a message on the error stack
  !Input:
  !       -msg: the message given by a subroutine name and additional info
  subroutine tensor_stack_push(msg)
     implicit none
     character*(*), intent(in) :: msg
     integer :: i
     tensor_stack%c =  tensor_stack%c + 1

     if(tensor_stack%c > tensor_stack%n )then
        call tensor_stack_increase()
     endif
     write(tensor_stack%s(tensor_stack%c),*)trim(msg)
     tensor_stack%s(tensor_stack%c) = adjustl(tensor_stack%s(tensor_stack%c))
  end subroutine tensor_stack_push

  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   February, 2016
  !Intent: Increase the stack size
  subroutine tensor_stack_increase()
     implicit none
     integer :: i, oldn
     character(tensor_name_length), pointer :: tmp(:)

     if( .not. tensor_stack%i )then
        write(*,*) error_b//"tensor_stack_increase"//error_e//" stack is not initialized"
        stop 1
     endif

     ! Save old stack
     oldn = tensor_stack%n
     allocate(tmp(oldn))
     do i = 1, oldn
        tmp(i) = tensor_stack%s(i)
     enddo

     ! Increase stack
     deallocate(tensor_stack%s)
     tensor_stack%n = tensor_stack%n + 10
     allocate(tensor_stack%s(tensor_stack%n))

     ! Recover stack
     do i = 1, oldn
        tensor_stack%s(i) = tmp(i)
     enddo
     deallocate(tmp)
  end subroutine tensor_stack_increase

  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   February, 2016
  !Intent: Pop a message from the error stack
  subroutine tensor_stack_pop()
     implicit none
     integer :: i
     tensor_stack%c =  tensor_stack%c - 1
     if( tensor_stack%c < 0)then
        print *, error_b//"tensor_stack_pop"//error_e//" stack below 0"
        stop 1
     endif
  end subroutine tensor_stack_pop

  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   February, 2016
  !Intent: Print the current stack
  subroutine tensor_print_stack()
     implicit none
     integer :: i
     if( tensor_stack%c > 1)then
        write(*,*)""
        write(*,*)"Tensor Stack:"
        write(*,*)"-------------"
        do i = tensor_stack%c, 1, -1
           write (*,'(i4,": ",A)')i,trim(tensor_stack%s(i))
        enddo
     endif
  end subroutine tensor_print_stack

  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   February, 2016
  !Intent: quit the tensors because of an error
  !Input:
  !       -msg:  the error message
  !       -stat: the error status (usually meaningless)
  subroutine tensor_status_quit_std(msg,stat)
     integer(tensor_standard_int), intent(in) :: stat
     include "error_output.inc"
  end subroutine tensor_status_quit_std

  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   February, 2016
  !Intent: quit the tensors because of an error
  !Input:
  !       -msg:  the error message
  !       -stat: the error status (usually meaningless)
  subroutine tensor_status_quit_long(msg,stat)
     integer(tensor_long_int), intent(in) :: stat
     include "error_output.inc"
  end subroutine tensor_status_quit_long

  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   February, 2016
  !Intent: print a warning
  !Input:
  !       -msg:  the message
  subroutine tensor_warning(msg)
     character*(*),intent(in) :: msg
     print *, warning_b//trim(tensor_stack%s(tensor_stack%c))//error_e//adjustl(trim(msg))
#ifdef VAR_DEBUG
     call tensor_print_stack()
#endif
  end subroutine tensor_warning
end module tensor_error_handler
