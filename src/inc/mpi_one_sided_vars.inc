       integer(tensor_long_int), intent(in) :: pos
       integer(tensor_mpi_kind), intent(in) :: dest
       integer(tensor_mpi_kind), intent(in) :: win
       logical, intent(in), optional             :: fl
       integer(tensor_mpi_kind)  :: ierr, nMPI, datatype
       integer(tensor_long_int)  :: n, chunk, first_el
       integer(MPI_ADDRESS_KIND) :: offset
       logical :: f
