      integer(tensor_standard_int), intent(in) :: idx
      integer, intent(out), optional                :: stat
      integer :: alloc_stat 
      integer(tensor_long_int) :: b,n
      call tensor_stack_push(srname)

      alloc_stat = 0
      n          = size(p)

      !bytes are the number of elements times the size
      b = n * counters(idx)%size_

      deallocate(p,stat=alloc_stat)

      !$OMP CRITICAL
      counters(idx)%curr_ = counters(idx)%curr_ - b
      if(associated(tensor_counter_ext_mem)) tensor_counter_ext_mem = tensor_counter_ext_mem - b
      !$OMP END CRITICAL
         

      if( alloc_stat /= 0 ) then
         if(.not. present(stat))then
            call tensor_status_quit("deallocation failed",alloc_stat)
         else
            stat = alloc_stat
         endif
      endif

      p => null()
      call tensor_stack_pop()
