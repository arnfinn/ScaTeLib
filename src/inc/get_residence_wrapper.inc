
     integer(tensor_long_int) :: rank8
     integer(tensor_long_int) :: globaltilenumber8
     integer(tensor_long_int) :: pos8
     integer(tensor_long_int) :: idx8
     integer(tensor_long_int) :: widx8
     integer(tensor_long_int) :: nnod8
     integer(tensor_long_int) :: offs8
     integer(tensor_long_int) :: tsze8

     globaltilenumber8 = globaltilenumber
     nnod8             = tensor_get_nnod(arr)
     offs8             = tensor_get_offset(arr)
     tsze8             = tensor_get_tsze(arr)

     call get_residence_of_tile_basic(nnod8,offs8,tsze8,globaltilenumber8,rank8,pos8,idx8,widx8)

     rank = rank8
     idx  = idx8
     pos  = pos8
     widx = widx8
