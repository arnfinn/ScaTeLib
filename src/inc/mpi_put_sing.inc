       ierr        = 0
       n           = 1
       f           = .false.
       if(present(fl)) f = fl

       datatype    = stats%d_mpi

       stats%bytes = stats%bytes + stats%size_ * n
       stats%time_ = stats%time_ - mpi_wtime()

       nMPI     = 1
       first_el = 1

       offset = int(first_el-1+pos-1,kind=MPI_ADDRESS_KIND)

       call mpi_put(buffer,nMPI,datatype,dest,offset,nMPI,datatype,win,ierr)

       if (ierr/= 0) call tensor_status_quit("mpi returns ierr=",ierr)

       if( f ) call tensor_mpi_win_flush(win,local=.true.)

       stats%time_ = stats%time_ + mpi_wtime()
