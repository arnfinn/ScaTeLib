!@file: this file contains the interfaces necessary for getting the tiles
module TiledDistributedTensor_get_tile_module
use tensor_type_def_module
  use,intrinsic :: iso_c_binding,only:c_f_pointer,c_loc

  use get_idx_mod
  use tensor_parameters_module
  use tensor_counters_module
  use tensor_mpi_binding_module
  use tensor_bg_buf_module
  use tensor_buffer_handling_module
  use tensor_mpi_interface_module
  use tensor_mpi_operations_module
  use tensor_type_allocator_module

  use tensor_basic_module
  use tensor_pdm_basic_module
  use tensor_pdm_jobs_module

#ifdef VAR_TENSOR_CLASS
  use tensor_DenseTensor_type_module
  use tensor_TiledTensor_type_module
#endif

#ifdef VAR_MPI
  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   October 2015
  !Intent: this interface provides a simple access to the necessary routines for
  !        tile get operations
  interface TiledDistributedTensor_get_tile
     module procedure TiledDistributedTensor_gett44_c,&
        &TiledDistributedTensor_gett48_c,&
        &TiledDistributedTensor_gett84_c,&
        &TiledDistributedTensor_gett88_c,&
        &TiledDistributedTensor_gettile_modeidx_c
  end interface TiledDistributedTensor_get_tile

  real(tensor_dp), save :: time_pdm_get          = 0.0E0_tensor_dp
  integer(tensor_long_int), save :: bytes_transferred_get = 0
  integer(tensor_long_int), save :: nmsg_get = 0
#endif

#ifdef VAR_MPI
  public :: TiledDistributedTensor_get_tile
  public :: bytes_transferred_get,time_pdm_get,nmsg_get
  public :: TiledDistributedTensor_gett88_c !, TiledDistributedTensor_gett88_t
#endif
  private
  contains

#ifdef VAR_MPI
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !!!!!!!                   GET TILES
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   October 2015
  !Intent: Put a tile for a Tiled Distributedensor. The tile data is 
  !        given as a fortran array and a remote update of the tensor is 
  !        performed with this data
  !Input:
  !       - modidx:   the modal tile index
  !       - T:        the TiledDistributedTensor
  !       - nelms:    number of elements in the tile
  !       - flush_it: flush the window after the call to accumulate
  !       - req:      request handle for MPI_RGET rater than
  !                   MPI_GET
  !Output:
  !       - fort:     the tile data
  subroutine TiledDistributedTensor_gettile_modeidx_c(T,modidx,fort,nelms,flush_it,req)
     implicit none
     type(TiledDistributedTensor),intent(in) ::T
     integer,intent(in) :: modidx(:),nelms
     real(tensor_dp),intent(inout) :: fort(nelms)
     logical, optional, intent(in) :: flush_it
     integer(tensor_mpi_kind),intent(inout),optional :: req
     logical :: ls
     integer(tensor_long_int) :: cidx
     cidx=get_cidx(modidx,T%ntpm,T%mode)
     call TiledDistributedTensor_gettile_combidx(T,cidx,fort,int(nelms,kind=tensor_long_int),flush_it=flush_it,req=req)
  end subroutine TiledDistributedTensor_gettile_modeidx_c

  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   October 2015
  !Intent: Put a tile for a Tiled Distributedensor. The tile data is 
  !        given as a fortran array and a remote update of the tensor is 
  !        performed with this data
  !Input:
  !       - globtilenr:   the tile index
  !       - T:        the TiledDistributedTensor
  !       - nelms:    number of elements in the tile
  !       - flush_it: flush the window after the call to accumulate
  !       - req:      request handle for MPI_RGET rater than
  !                   MPI_GET
  !Output:
  !       - fort:     the tile data
  subroutine TiledDistributedTensor_gett88_c(T,globtilenr,fort,nelms,flush_it,req)
     implicit none
     type(TiledDistributedTensor),intent(in) :: T
     integer(tensor_long_int),intent(in) :: globtilenr
     integer(tensor_long_int),intent(in) :: nelms
     real(tensor_dp),intent(inout) :: fort(nelms)
     logical, optional, intent(in) :: flush_it
     integer(tensor_mpi_kind),intent(inout),optional :: req
     call TiledDistributedTensor_gettile_combidx(T,int(globtilenr,kind=tensor_long_int),&
        &fort,int(nelms,kind=tensor_long_int),flush_it=flush_it,req=req)
  end subroutine TiledDistributedTensor_gett88_c

  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   October 2015
  !Intent: Put a tile for a Tiled Distributedensor. The tile data is 
  !        given as a fortran array and a remote update of the tensor is 
  !        performed with this data
  !Input:
  !       - globtilenr:   the tile index
  !       - T:        the TiledDistributedTensor
  !       - nelms:    number of elements in the tile
  !       - flush_it: flush the window after the call to accumulate
  !       - req:      request handle for MPI_RGET rater than
  !                   MPI_GET
  !Output:
  !       - fort:     the tile data
  subroutine TiledDistributedTensor_gett48_c(T,globtilenr,fort,nelms,flush_it,req)
     implicit none
     type(TiledDistributedTensor),intent(in) :: T
     integer(tensor_standard_int),intent(in) :: globtilenr
     integer(tensor_long_int),intent(in) :: nelms
     real(tensor_dp),intent(inout) :: fort(nelms)
     logical, optional, intent(in) :: flush_it
     integer(tensor_mpi_kind),intent(inout),optional :: req
     call TiledDistributedTensor_gettile_combidx(T,int(globtilenr,kind=tensor_long_int),&
        &fort,int(nelms,kind=tensor_long_int),flush_it=flush_it,req=req)
  end subroutine TiledDistributedTensor_gett48_c

  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   October 2015
  !Intent: Put a tile for a Tiled Distributedensor. The tile data is 
  !        given as a fortran array and a remote update of the tensor is 
  !        performed with this data
  !Input:
  !       - globtilenr:   the tile index
  !       - T:        the TiledDistributedTensor
  !       - nelms:    number of elements in the tile
  !       - flush_it: flush the window after the call to accumulate
  !       - req:      request handle for MPI_RGET rater than
  !                   MPI_GET
  !Output:
  !       - fort:     the tile data
  subroutine TiledDistributedTensor_gett84_c(T,globtilenr,fort,nelms,flush_it,req)
     implicit none
     type(TiledDistributedTensor),intent(in) :: T
     integer(tensor_long_int),intent(in) :: globtilenr
     integer(tensor_standard_int),intent(in) :: nelms
     real(tensor_dp),intent(inout) :: fort(nelms)
     logical, optional, intent(in) :: flush_it
     integer(tensor_mpi_kind),intent(inout),optional :: req
     call TiledDistributedTensor_gettile_combidx(T,int(globtilenr,kind=tensor_long_int),&
        &fort,int(nelms,kind=tensor_long_int),flush_it=flush_it,req=req)
  end subroutine TiledDistributedTensor_gett84_c

  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   October 2015
  !Intent: Put a tile for a Tiled Distributedensor. The tile data is 
  !        given as a fortran array and a remote update of the tensor is 
  !        performed with this data
  !Input:
  !       - globtilenr:   the tile index
  !       - T:        the TiledDistributedTensor
  !       - nelms:    number of elements in the tile
  !       - flush_it: flush the window after the call to accumulate
  !       - req:      request handle for MPI_RGET rater than
  !                   MPI_GET
  !Output:
  !       - fort:     the tile data
  subroutine TiledDistributedTensor_gett44_c(T,globtilenr,fort,nelms,flush_it,req)
     implicit none
     type(TiledDistributedTensor),intent(in) :: T
     integer(tensor_standard_int),intent(in) :: globtilenr
     integer(tensor_standard_int),intent(in) :: nelms
     real(tensor_dp),intent(inout) :: fort(nelms)
     logical, optional, intent(in) :: flush_it
     integer(tensor_mpi_kind),intent(inout),optional :: req
     call TiledDistributedTensor_gettile_combidx(T,int(globtilenr,kind=tensor_long_int),&
        &fort,int(nelms,kind=tensor_long_int),flush_it=flush_it,req=req)
  end subroutine TiledDistributedTensor_gett44_c

  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   October 2015
  !Intent: Put a tile for a Tiled Distributedensor. The tile data is 
  !        given as a fortran array and a remote update of the tensor is 
  !        performed with this data
  !Input:
  !       - globtilenr:   the tile index
  !       - T:      the TiledDistributedTensor
  !       - nelms:    number of elements in the tile
  !       - flush_it: flush the window after the call to accumulate
  !       - req:      request handle for MPI_RGET rater than
  !                   MPI_GET
  !Output:
  !       - fort:     the tile data
  subroutine TiledDistributedTensor_gettile_combidx(T,globtilenr,fort,nelms,flush_it,req)
     implicit none
     class(TiledDistributedTensor),intent(in) :: T
     integer(tensor_long_int),intent(in) :: globtilenr
     integer(tensor_long_int),intent(in) :: nelms
     real(tensor_dp),intent(inout) :: fort(nelms)
     logical, optional, intent(in) :: flush_it
     integer(tensor_mpi_kind),intent(inout),optional :: req
     integer(tensor_mpi_kind) :: source,r
     integer(tensor_standard_int) :: gt
     integer(tensor_long_int) :: nnod8, offs8, tsze8, gt8, node8, pos8, idx8, widx8
     logical :: ls
#ifdef VAR_MPI
     integer(tensor_long_int) :: p, dpos, widx
#ifdef TENSORS_IN_LSDALTON
     call time_start_phase( PHASE_COMM )
#endif

     gt = globtilenr

     nnod8 = T%mpi%nnod
     offs8 = T%mpi%offset
     tsze8 = T%tsize
     gt8   = globtilenr

     call get_residence_of_tile_basic(nnod8,offs8,tsze8,gt8,node8,pos8,idx8,widx8)

     source = node8
     dpos   = pos8
     p      = idx8
     widx   = widx8

     ls     = T%lock_set(widx)

     time_pdm_get = time_pdm_get - mpi_wtime()

     if(.not.ls)call tensor_mpi_win_lock(source,T%Wi(widx),'s')

     if(present(req))then
        call tensor_mpi_get(fort,nelms,p,source,T%wi(widx),req, fl = flush_it)
     else
        call tensor_mpi_get(fort,nelms,p,source,T%wi(widx), fl = flush_it)
     endif

     if(present(flush_it))then
        if(flush_it) call tensor_mpi_win_flush(T%wi(widx), rank=source, local=.false.)
     endif

     if(.not.ls)call tensor_mpi_win_unlock(source,T%wi(widx))

     time_pdm_get = time_pdm_get + mpi_wtime()
     bytes_transferred_get = bytes_transferred_get + nelms * tensor_dp
     nmsg_get = nmsg_get + 1

#ifdef TENSORS_IN_LSDALTON
     call time_start_phase( PHASE_WORK )
#endif
#endif
  end subroutine TiledDistributedTensor_gettile_combidx

   !NO MPI
#else
   ! Dummy routine if non-mpi build to avoid a warning
   subroutine TiledDistributedTensor_get_tile_dummy_non_mpi()
      implicit none
   end subroutine TiledDistributedTensor_get_tile_dummy_non_mpi
#endif

end module TiledDistributedTensor_get_tile_module
