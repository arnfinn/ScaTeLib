!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Author:   Patrick Ettenhuber (pettenhuber@gmail.com)
! Date:     February, 2016
! File:     This file contains a simple example of how to use tensor IO 
! License : This file is subject to all the licensing conditions under which
!           ScaTeLib is distributed
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! Compile: where ../../build has to be replaced by your specific build path and
! the arguments in parenthesis are only necessary if you compiled ScaTeLib with
! these, conversely this means that you need to provide every lib you used for
! compiling ScaTeLib when linking this example:

! Compile: mpif90 -I../../build/modules  -c example2.F90 -o example2mpi.F90.o
! Link:    mpif90 ../../build/src/CMakeFiles/ScaTeLib.dir/tensor_interface.F90.o example2mpi.F90.o -o example2mpi.x  ../../build/src/libScaTeLib.a ( -framework Accelerate -fopenmp )

program simple_tensor_use
   use mpi
   use tensor_interface_module
   implicit none
   type(tensor)    :: A,B,C
   integer(kind=8) :: bytes_used
   integer :: io_err
   integer, parameter :: mpi32o64bit = 4

   !If you have used a 64bit MPI library to compile ScaTeLib use the following line in
   !integer, parameter :: mpi32o64bit = 8
   integer(kind=mpi32o64bit) :: ierr, me, nn

   !Initialize MPI before you Initialize the tensors
   call mpi_init(ierr)
   call mpi_comm_rank(mpi_comm_world,me,ierr)
   call mpi_comm_size(mpi_comm_world,nn,ierr)

   print *,"on rank",me,"of",nn,"Before initialization, bytes_used:   ",bytes_used

   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Tensor operations will not work here !
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   !Initialize the tensor intrerface
   call tensor_initialize_interface(mpi_comm_world, mem_ctr = bytes_used)


   !please feel free to test any tensor operation here!
   call tensor_ainit(A,[140,144,124,180],Ttype=TT_TiledDistributedTensor,local=(nn==1),tdim=[20,19,18,17])

   ! Give the tensor a label, all (only) alphanumerical characters bcome the
   ! file name with the extension .tns
   call tensor_set_label(A,"This becåmes my file name")

   ! set some data
   call tensor_random(A)

   call print_norm(A,"This is my random norm:",print_=(me==0))

   ! write the file
   io_err = tensor_write(A)

   call tensor_free(A)

   ! Now initialize tensor B with the same dimensions and read the file. Note
   ! that the tile dimensions now are different. The tensor is thus distributed
   ! differently than before but the data is the same. Indeed, also a dense
   ! tensor could be declared here and the code would still work, try it!
   call tensor_ainit(B,[140,144,124,180],Ttype=TT_TiledDistributedTensor,local=(nn==1),tdim=[8,9,10,11])

   ! Give the tensor a label, all alphanumerical characters
   io_err = tensor_read(B,fname="Thisbecmesmyfilename.tns")

   ! Check the label
   print *, "The original tensor was labeled:",tensor_get_label(B)
   call print_norm(B,"This and had the norm: ",print_=(me==0))

   ! The file is not automatically deleted when a tensor is freed (for obvious
   ! reasons)
   io_err = tensor_delete_file(B)

   !Free B
   call tensor_free(B)


   ! Finalize the tensor interface
   call tensor_finalize_interface

   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Tensor operations will not work here !
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   !Finalize MPI after you Initialize the tensors
   call mpi_finalize(ierr)

end program simple_tensor_use
